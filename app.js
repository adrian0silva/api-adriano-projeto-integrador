const app = require("express")();
const mongoose = require("mongoose");
const bodyParser = require("body-parser"); 

app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/bugph",{
   useNewUrlParser: true
},() => {
    console.log("Banco de dados connectado!")
})
require("./models/Users");
const Users = mongoose.model("Users");

app.post("/users",(req,res) => {
    var users = new Users({
        nome: req.body.nome,
        email: req.body.email,
        senha: req.body.senha
    });
    users.save().then(() => {
        
        res.statusCode = 201
        res.send();
    }).catch((erro) => {
        if(erro) { 
            throw erro;
        }
       
        res.statusCode = 417;
        res.send();
    })
})

app.get("/users",(req,res) => {
    Users.find({},(erro,dados) => {
        if(erro) {
            res.statusCode = 417;
            res.send()
        }

        res.json(dados);
    })
})

   
app.get("/users/:id",(req,res) => {
    Users.findById(req.params.id).then((users) => {
        res.statusCode = 200;
        res.json(users)
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
});


require("./models/Curtidas");
const Curtidas = mongoose.model("Curtidas");

app.post("/curtidas",(req,res) => {
    var curtidas = new Curtidas({
        id_usuario: req.body.id_usuario,
        id_post: req.body.id_post,
        data_hora: req.body.data_hora
    });
    curtidas.save().then(() => {
     
        res.statusCode = 201
        res.send();
    }).catch((erro) => {
        if(erro) { 
            throw erro;
        }
        
        res.statusCode = 417;
        res.send();
    })
})

app.get("/curtidas",(req,res) => {
    Curtidas.find({},(erro,dados) => {
        if(erro) {
            res.statusCode = 417;
            res.send()
        }

        res.json(dados);
    })
})

app.get("/curtidas/:id",(req,res) => {
    Curtidas.findById(req.params.id).then((curtidas) => {
        res.statusCode = 200;
        res.json(curtidas)
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
});


require("./models/Posts");
const Posts = mongoose.model("Posts");

app.post("/posts",(req,res) => {
    var posts = new Posts({
        id: req.body.id, 
        id_usuario: req.body.id_usuario,
        titulo: req.body.titulo,
        imagem: req.body.imagem
    });
    posts.save().then(() => {
      
        res.statusCode = 201
        res.send();
    }).catch((erro) => {
        if(erro) { 
            throw erro;
        }
        res.statusCode = 417;
        res.send();
    })
})

app.get("/posts",(req,res) => {
    Posts.find({},(erro,dados) => {
        if(erro) {
            res.statusCode = 417;
            res.send()
        }

        res.json(dados);
    })
})

app.get("/posts/:id",(req,res) => {
    Posts.findById(req.params.id).then((posts) => {
        res.statusCode = 200;
        res.json(posts)
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
});

app.put("/posts/:id",(req,res) => {
    Posts.findByIdAndUpdate(req.params.id,req.body,{new: true},(erro, dados) => {
            if (erro) return res.status(500).send(erro);
            return res.send(dados);
        }
    )
})

app.delete("/posts/:id",(req,res) => {
    Posts.findByIdAndRemove(req.params.id).then((posts) => {
        if(users) {
            res.statusCode = 200;
            res.send();
        } else {
            res.statusCode = 404;
            res.send();
        } 
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
})


require("./models/Logs");
const Logs = mongoose.model("Logs");

app.post("/logs",(req,res) => {
    var logs = new Logs({
        id: req.body.id, 
        id_usuario: req.body.id_usuario,
        log: req.body.log,
        data_hora: req.body.data_hora
    });
    logs.save().then(() => {
        res.statusCode = 201
        res.send();
    }).catch((erro) => {
        if(erro) { 
            throw erro;
        }
        res.statusCode = 417;
        res.send();
    })
})

app.get("/logs",(req,res) => {
    Logs.find({},(erro,dados) => {
        if(erro) {
            res.statusCode = 417;
            res.send()
        }

        res.json(dados);
    })
})
app.get("/logs/:id",(req,res) => {
    Logs.findById(req.params.id).then((logs) => {
        res.statusCode = 200;
        res.json(logs)
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
});

app.put("/logs/:id",(req,res) => {
    Logs.findByIdAndUpdate(req.params.id,req.body,{new: true},(erro, dados) => {
            if (erro) return res.status(500).send(erro);
            return res.send(dados);
        }
    )
})


app.delete("/logs/:id",(req,res) => {
    Logs.findByIdAndRemove(req.params.id).then((logs) => {
        if(users) {
            res.statusCode = 200;
            res.send();
        } else {
            res.statusCode = 404;
            res.send();
        } 
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
})

require("./models/Comentarios");
const Comentarios = mongoose.model("Comentarios");

app.post("/comentarios",(req,res) => {
    var comentarios = new Comentarios({
        id: req.body.id, 
        id_usuario: req.body.id_usuario,
        id_post: req.body.id_post,
        comentario: req.body.comentario,
        data_hora: req.body.data_hora
    });
    comentarios.save().then(() => {
        res.statusCode = 201
        res.send();
    }).catch((erro) => {
        if(erro) { 
            throw erro;
        }
        res.statusCode = 417;
        res.send();
    })
})

app.get("/comentarios",(req,res) => {
    Comentarios.find({},(erro,dados) => {
        if(erro) {
            res.statusCode = 417;
            res.send()
        }

        res.json(dados);
    })
})

app.get("/comentarios/:id",(req,res) => {
    Comentarios.findById(req.params.id).then((comentarios) => {
        res.statusCode = 200;
        res.json(comentarios)
    }).catch((erro) => {
        if(erro) {
            res.statusCode = 417;
            res.send();
            throw erro;
        }
    });
});


app.listen(8000,() => {
    console.log("API rodando")
})

